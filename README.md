# TwitterFollowersIntersection

This app calculates the intersection between the friends for two or more twitter accounts and starts to follow the common ones.

##Configuration
First of all, configure your db connection in the `.env` file.
```
DB_CONNECTION=
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

If you want to configure a different twitter app for the application, you only need to change the following values in the `.env` file

```
TWITTER_CONSUMER_KEY=
TWITTER_CONSUMER_SECRET_KEY=
```

##Create database schema

The whole schema is defined in outstanding migrations, execute the migrate Artisan command:
```
composer update
npm update
php artisan migrate
```

##Local Development Server

If you have PHP installed locally and you would like to use PHP's built-in development server to serve your application, you may use the serve Artisan command. This command will start a development server at `http://localhost:8000`:
```
php artisan serve
```

