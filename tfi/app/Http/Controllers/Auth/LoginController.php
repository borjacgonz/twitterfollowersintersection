<?php

namespace App\Http\Controllers\Auth;

require "../vendor/autoload.php";
use Abraham\TwitterOAuth\TwitterOAuth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Session;
use Log;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider()
    {
        $connection = new TwitterOAuth(
            env('TWITTER_CONSUMER_KEY'), 
            env('TWITTER_CONSUMER_SECRET_KEY'));
        $oauth_token = $connection->oauth("oauth/request_token", 
            ['oauth_callback' => 'http://127.0.0.1:8000/login/twitter/callback/']);
        $url = $connection->url('oauth/authorize', ['oauth_token' => $oauth_token['oauth_token']]);
        return redirect($url);

    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback() 
    {
        Session::forget('oauth_token');
        Session::forget('oauth_token_secret');

        Log::info("Verifiy with token: ".$_GET['oauth_token']);
        Log::info("Verifiy with verifier: ". $_GET['oauth_verifier']);
        $connection = new TwitterOAuth(
            env('TWITTER_CONSUMER_KEY'), 
            env('TWITTER_CONSUMER_SECRET_KEY'), 
            $_GET['oauth_token'], 
            $_GET['oauth_verifier']);
        $accessToken = $connection->oauth("oauth/access_token", 
            ['oauth_verifier' => $_GET['oauth_verifier']]);

        Log::info("Acces token: ".json_encode($accessToken));

        Session::put('oauth_token', $accessToken['oauth_token']);
        Session::put('oauth_token_secret', $accessToken['oauth_token_secret']);
        Session::save();
        $userName = $accessToken['screen_name'];
        $user = User::where('name', $userName)->first();
        if(!$user) {
            $user = User::create(['name' => $userName]); 
        }

        auth()->login($user);
        return redirect()->route('home');
    } 
}
