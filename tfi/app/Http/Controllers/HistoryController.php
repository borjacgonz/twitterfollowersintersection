<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Log;
use App\Search;
use App\Follow;

class HistoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the history dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function history()
    {
        return view('history');
    }

    public function historySearches()
    {
        return Search::leftJoin('follows', 'searches.id', '=', 'follows.search_id')
            ->where('searches.user_id', auth()->id())
            ->selectRaw('searches.*, count(follows.id) as followCount')
            ->groupBy('searches.id')
            ->get();
    }

    public function lastSearches()
    {
        return Search::where('user_id', auth()->id())->get();
    }

    public function lastFollows()
    {
        return Follow::leftJoin('searches', 'searches.id', '=', 'follows.search_id')
            ->where('searches.user_id', auth()->id())
            ->get();
    }
}
