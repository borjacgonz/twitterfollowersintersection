<?php

namespace App\Http\Controllers;

require "../vendor/autoload.php";
use Abraham\TwitterOAuth\TwitterOAuth;
use Session;
use Illuminate\Http\Request;
use Log;
use App\Search;
use App\Follow;

class TwitterController extends Controller
{

    private $connection;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Calculates the common list of friends of an array of twitter accounts.
     *
     * @return The friends that the accounts have in common.
     */
    public function mix()
    {
        $friends = array();
        foreach (explode(',', $_GET['accounts']) as $account)
        {
            array_push($friends, $this->getFriends($account)); 
        }
        $commonFriends = $this->getCommonFriends($friends);

        $search = $this->saveSearch($_GET['accounts'],count($commonFriends));
        Session::put('search', $search);

        return json_encode(array_values($commonFriends));
    }

    function getFriends($account) 
    {
        $friends = array();
        $page = -1;
        while ($page != 0) 
        {
            Log::info("Looking for friends ".$account);
            $data = $this->fetchConnecion()->get("friends/list", 
                    ["screen_name" => $account, "count" => 200, "cursor"=> $page]);
            $data = json_decode(json_encode($data), true);
            $page = $data['next_cursor'];
            $friends = array_merge($friends, $data['users']);
        }

        return $friends;
    }

    function follow(Request $request) 
    {
        foreach ($request->names as $name)
        {
            Log::info("Before following ".$name);
            $response = $this->fetchConnecion()->post("friendships/create", 
                    ["screen_name" => $name, "follow" => true]);
            Log::info("Response following ".$name." - ".json_encode($response));
            $this->saveFollow($name);
        }
    }

    function getCommonFriends($friends) 
    {
        $commonFriends = $this->setIdAsKey($friends[0]);
        $friendsByAccounts = array_slice($friends, 1);
        foreach ($friendsByAccounts as $friends)
        {
            $commonFriends = array_intersect_key($commonFriends, $this->setIdAsKey($friends));
        }
        return $commonFriends;
    }

    function setIdAsKey($array) 
    {
        $result = array();
        foreach($array as $field) 
        {

          $result[$field['id']] = $field;
        }
        return $result;
    }

    function saveSearch($accounts, $results) 
    {
        $search = new Search();
        $search->names   = $accounts;
        $search->results = $results;
        $search->user_id = auth()->id();
        $search->save();
        return $search;
    }

    function saveFollow($name) 
    {
        $follow = new Follow();
        $follow->screen_name = $name;
        $follow->search_id = Session::get('search')->id;
        $follow->save();
        return $follow;
    }

    function fetchConnecion() 
    {
        if ($this->connection == null)
        {
            Log::info("Initialize connection with token ".Session::get('oauth_token'));
            $this->connection = new TwitterOAuth(
                env('TWITTER_CONSUMER_KEY'), 
                env('TWITTER_CONSUMER_SECRET_KEY'),
                Session::get('oauth_token'),
                Session::get('oauth_token_secret')
            );
        }
        return $this->connection;
    }

}
